package com.jiagouedu.core.freemarker.fn;

import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
import com.jiagouedu.core.front.SystemManager;

import java.util.List;

/**
 * 获取系统管理
 * @author zdp072
 */
public class SystemManagerGetter implements TemplateMethodModelEx {
    @Override
    public Object exec(List arguments) throws TemplateModelException {
        return SystemManager.getInstance();
    }
}
